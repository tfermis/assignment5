package assignment5;

import java.util.List;

/**
 * Assignment5Interface class for Assignment 5. 
 * 
 * @author EE422C TAs
 * @version 1.0
 */
public interface Assignment5Interface
{
    /**
     * Computes the word ladder from startWord to endWord.
     * 
     * @param startWord The starting word in the word ladder.
     * @param endWord The ending word in the word ladder.
     * @return A list of strings that represents the word ladder. The 0th index should contain
     * the startWord and the last position should contain endWord. All intermediate words should
     * be different by exactly one letter.
     * @throws NoSuchLadderException is thrown if no word ladder can be generated from startWord and endWord.
     * 	if startWord or endWord is invalid (not in dictionary) then message is "One of the two words (startWord, endWord) is not in the dictionary."
     * 	else the message is "No such word ladder found"
     */
    List<String> computeLadder(String startWord, String endWord) throws NoSuchLadderException;

    /**
     * Determines whether or not a word ladder is valid. NOTE: this method is NOT part of the requirements
     * of the lab, but it would be a great idea to implement it so you can programatically verify if the word ladder
     * you return in the above method is correct.
     * For a word ladder to be valid the 0th index must be startWord, the endWord must be in the last index position,
     * and all intermediate words need to be exactly one distance apart.
     * @param startWord The starting word in the word ladder.
     * @param endWord The ending word in the word ladder.
     * @param wordLadder The wordLadder to check if the solution is valid
     * @return True if the word ladder is correct, false otherwise.
     */
    boolean validateResult(String startWord, String endWord, List<String> wordLadder);
}
