/* Assignment 5
 * Names: Rohan Nagar, Margret Tumbokon, Tom Ermis
 * Section: Thursday 3:30 - 5
 */

package assignment5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

/**
 * Driver class for Assignment 5. 
 * 
 * @author Rohan Nagar, Margret Tumbokon, Tom Ermis
 * @version 1.0
 */
public class A5Driver
{
	private static final String filename = "assn5data.txt";

   public static void main(String[] args)
   {
        /* Create a word ladder solver object */
        Assignment5Interface wordLadderSolver = new WordLadderSolver();

        /* initialize a dictionary and stop watch */
        Dictionary dict = new Dictionary();
        StopWatch watch = new StopWatch();
        
        /* Open file */
        Scanner file;
        try
        {
        	file = new Scanner(new File(filename));
        } catch (FileNotFoundException e)
        { // catch FileNotFound. Print err message and exit
        	System.out.println("File " + filename + " not found.");
        	System.out.println("Please add the file to the directory or change the filename class variale in A5Driver.java");
        	System.exit(1);
        	return;
        }
        
        watch.start(); // start watch after file IO

        /* For each line in the file, compute the word ladder */
        while(file.hasNextLine())
        {
	        String startWord = file.next();
	        String endWord = file.next();

	        /* Print the words being searched */
	        System.out.println("For the input words " + startWord + " and " + endWord);

	        /* make sure both words are in the dictionary
	         * if they're not, print err message and continue to next pair */
	        if(!dict.contains(startWord) || !dict.contains(endWord))
	        {
	        	System.out.println("At least one of the words " + startWord + " and " + endWord + " are not legitimate 5-letter words from the dictionary.");
	        	System.out.println("**********");
				continue;
	        }

	        /* Compute the word ladder and print it */
	        try
	        {
	      	  List<String> result = wordLadderSolver.computeLadder(startWord, endWord);
	      	  boolean correct = wordLadderSolver.validateResult(startWord, endWord, result); // make sure result works
	      	  if(correct)
	      	  { printLadder(result); }
	        } 
	        catch (NoSuchLadderException e)
	        { // no ladder found, print err message
	      	  System.out.println("There is no word ladder between " + startWord + " and " + endWord + "!");
	      	  System.out.println("**********");
	        }
        }
        
        /* Stop the watch and report time taken */
        watch.stop();
        System.out.println("Program execution time in nanoseconds is: " + watch.getElapsedTime());
        System.out.println("Program execution time in seconds is: " + watch.getElapsedTime()/StopWatch.NANOS_PER_SEC);

        file.close();
    }

    /**
     * Prints out a word ladder to the console.
     * @param ladder The List of String objects that make up the ladder
     */
    public static void printLadder(List<String> ladder)
    {
    	System.out.println("The following word ladder was found:");
    	
    	for(String s : ladder)
    	{
    		System.out.println(s + " ");
    	}
    	
    	System.out.println();
    	System.out.println("**********");
    }
}
