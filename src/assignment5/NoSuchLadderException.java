package assignment5;

/**
 * NoSuchLadderException class for Assignment 5. 
 * 
 * @author EE422C TAs
 * @version 1.0
 */
public class NoSuchLadderException extends Exception
{
    private static final long serialVersionUID = 1L;

    public NoSuchLadderException(String message)
    {
        super(message);
    }

    public NoSuchLadderException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}
