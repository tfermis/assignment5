/* Names: Rohan Nagar, Margret Tumbokon, Tom Ermis
 * Section: Thursday 3:30 - 5
 */

package assignment5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner; 
import java.util.Set;

/**
 * Dictionary uses "assn5word.dat" to create a dictionary object containing those words 
 * @assumes assn5words.dat only contains 1 word per line and will trim any non-letters from that line before adding word
 * @assumes every line in assn5words.dat either starts with "*" or with a word
 * Word == letters only 
 * Useful for checking if a word is in the dictionary
 * 
 * @author Rohan Nagar, Margret Tumbokon, Tom Ermis
 * @version 1.0
 */
public class Dictionary
{
	/* Class members */
	private static Set<String> dictionary;
	private final static String sourceFile = "assn5words.dat";

	static // initializes dictionary based on sourceFile
	{
		dictionary = new HashSet<String>();
		Scanner sc;
		try
		{
			sc = new Scanner(new File(sourceFile));
			// read every line in input file and add to dictionary the word in that line
			while(sc.hasNext())
			{
				String curr = sc.nextLine();
				if(curr.charAt(0) != '*')
				{
					curr = curr.toLowerCase();
					curr = curr.replaceAll("[^a-z]","");
					if(!curr.equals("")) // making sure not to add empty strings
					{ dictionary.add(curr); }
				}
				
			}
			
			sc.close();
		} 
		catch (FileNotFoundException e)
		{
			System.out.println("Need assn5word.dat file. The dictionary is empty since the file was not found.");
		}

	}

	/**
	 * Non-case sensitive check to see if a word is in dictionary
	 * @param toFind The String object to find
	 * @return true if word to find is in dictionary, false otherwise
	 */
	public boolean contains(String toFind)
	{
		//toFind.toLowerCase();
		if(dictionary.contains(toFind))
		{return true;}
		else
		{return false;}
	}
	
}
