/* Assignment 5
 * Names: Rohan Nagar, Margret Tumbokon, Tom Ermis
 * Section: Thursday 3:30 - 5
 */

package assignment5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;


/**
 * This class should be used when wanting to find a word ladder between two words.
 * 
 * @author Rohan Nagar, Margret Tumbokon, Tom Ermis
 * @version 1.0
 */
public class WordLadderSolver implements Assignment5Interface
{
    /* Class members */
	private Dictionary dictionary; // Dictionary to check for valid words
	private Set<String> usedWords; // Holds all the words that have been checked so no duplicates show up

    /**
     * Default constructor. Initializes the dictionary object.
     */
	public WordLadderSolver()
	{
		dictionary = new Dictionary();
	}
	
    @Override
    public List<String> computeLadder(String startWord, String endWord) throws NoSuchLadderException //does this throws have to exist?
    {
    	
   	if(!dictionary.contains(startWord) || !dictionary.contains(endWord))
   	{throw new NoSuchLadderException("One of the two words (" + startWord +", " + endWord +") is not in the dictionary.");}
   	 
   	usedWords = new HashSet<String>(); // create empty Set on each call
    	List<String> wordLadder = new ArrayList<String>();
    	wordLadder = makeLadder(startWord, endWord, -1);
    	
    	// see if valid word ladder was found
   		if(!(wordLadder.isEmpty()))
   			return wordLadder;
   		else // if not, throw exception
   			throw new NoSuchLadderException("There is no word ladder between " + startWord + " and " + endWord + "!");
    }

    @Override
    public boolean validateResult(String startWord, String endWord, List<String> wordLadder)
    {
    		//2 for loops, charAt(i) == charAt(i)
   	   List <String> checkList = new ArrayList<String> ();	//assuming wordLadder is an arrayList
   	   int numPositionsOff = 0;
   	   checkList.add(startWord);
   	   checkList.addAll(wordLadder);
   	   checkList.add(endWord);
   	   for(int i = 0; i<checkList.size()-1; i++)		//super brute force algorithm of O(N^2)... hopefully not too bad
   	   {
   	  	 	String firstString = checkList.get(i);
   	  	 	String secondString = checkList.get(i+1);
   	  	 	for(int j = 0; j<firstString.length(); j++)
   	  		{
   	  	 		if(!(firstString.charAt(j) == secondString.charAt(j)))
   	 			{
   	  	 			numPositionsOff++;
   	  			}
   	  		}
   	 		
   	  		if(numPositionsOff>1)
      		{
   	  			return false;
  	   		}
  	   		numPositionsOff=0;
   	   }
   	   return true;
   	}


    /*
     * Creates a word ladder using a starting word and an ending word.
     * Recursive
     * @param startWord The word to start the ladder from
     * @param endWord The word to end the ladder at
     * @param pos The position that was most recently changed. This parameter should always be -1 when calling the function from outside of the method.
     * @return The List of String objects that make up the word ladder
     * O(?Recursion?)
     */
    private List<String> makeLadder(String startWord, String endWord, int pos)
    {
    	/* Create solution list and add startWord */
    	List<String> SolutionList = new ArrayList<String>();
    	SolutionList.add(startWord);
    	
    	/* If the two words are the same, the solution is simply that word */
    	if(startWord.equals(endWord))
    	{return SolutionList;}
    	
    	/* If the two words are off by one letter, the word ladder is the startWord followed by the endWord */
    	if(letterDifference(startWord, endWord)==1)
    	{
    		SolutionList.add(endWord);
    		return SolutionList;
    	}
    	
    	/* Find the rest of the ladder (recursive call) */
	   	SortedSet <String> tempList = generateTempList(startWord,endWord, pos); // generate possible candidates
	   	for(String s : tempList)
	   	{
	   		usedWords.add(s); // add each word to usedWords so that duplicate loops don't happen
	   		int posChanged = Integer.parseInt(s.substring(s.length()-1, s.length())); // position changed to get to this word is appended to end of string
	   		List<String> temp = makeLadder(s.substring(1,s.length()-1), endWord, posChanged); // recursive call
	   		
	   		if(!temp.isEmpty())
	   		{ // if the recursive call returned a non-empty list, that means the word ladder was valid
	   			SolutionList.addAll(temp);
	   			return SolutionList;
	   		}
	
	   	}
	   	
	   	/* Return empty list if no valid ladder from this word is found */
	   	return new ArrayList<String>();
    }


    /*
     * Generates a Set of all possible words that are one letter away from the input word.
     * Disregards the position that was most recently changed.
     * @param word The String object to examine
     * @param posChanged The most recent position changed in the String object
     * @return The Set containing all candidate words
     * O(n) probably
     */
    private SortedSet<String> generateTempList(String word, String endWord, int posChanged)
    {
    	SortedSet<String> list = new TreeSet<String>();

    	/* Go through each letter and see if words are valid */
    	for(int i = 0; i < word.length(); i++)
    	{
    		// don't change the position that was just changed
    		if(i != posChanged)
    		{
    			// change to every possible letter and check
        		for(char letter = 'a'; letter <= 'z'; letter++)
        		{
        			// make sure the same word isn't being checked
        			if(word.charAt(i) != letter)
        			{
        				String check = word.substring(0, i) + letter + word.substring(i+1); // change the letter
        				if(dictionary.contains(check) && !usedWords.contains(check))
        				{ // see if the new word is in the dictionary
        					usedWords.add(check); // add to usedWords so we know it's been used
        					// if it is, prepend the total letters away and add to the set
        					int prepend = letterDifference(check, endWord);
        					check = prepend + check.substring(0) + i; // also append position changed
        					list.add(check);
        				}
        			}

        		}

    		}


    	}

    	return list;
    }

	/*
	 *	Computes number of letter positions that the two given words differ by
	 *	@throws InputMismatchException if the two words are not the same length
	 * O(n)
	 */
	private int letterDifference(String word1, String word2)
	{
		if(word1.length() != word2.length())
		{throw new InputMismatchException();}

		int retval = 0;
		for(int i = 0; i < word1.length(); i++)
		{
			// if letters are not the same, then words differ by one more letter position (than before)
			if(word1.charAt(i) != word2.charAt(i))
			{retval ++;}
		}

		return retval;
	}
}
